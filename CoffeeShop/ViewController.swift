import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let shopCollectionViewCellId = "ShopCollectionViewCell"
    
    let shopName = ["New York Coffee", "Gloria Jeans", "Espresso", "Dunkin'Donuts"]
    let shopImage = [UIImage(named: "shop2"), UIImage(named: "shop3"), UIImage(named: "shop1"), UIImage(named: "shop4")]
    let shopDescription = ["Sindhi Muslim Cooperative Housing Society(smchs), Khi",
                               "Tipu Sultan Road PECHS, Karachi",
                               "Street A-199, Block A, SMCHS Karachi",
                               "Sharah-e-Sher Shah Suri, Block 'A', Karachi"]
    
    var shops = [ShopDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        registerCell()
        initData()
        
    }
    
    func registerCell() {
        let nibCell = UINib(nibName: shopCollectionViewCellId, bundle: nil)
        collectionView.register(nibCell, forCellWithReuseIdentifier: shopCollectionViewCellId)
    }
    
    func initData() {
        for item in 0...3 {
            let shop = ShopDetails()
            shop?.name = shopName[item]
            shop?.address = shopDescription[item]
            
            shops.append(shop!)
        }
        collectionView.reloadData()
    }

}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shops.count
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset: CGFloat = 10
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 300)
    }
 
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: shopCollectionViewCellId, for: indexPath) as! ShopCollectionViewCell 
        let shop = shops[indexPath.row]
        cell.image.image = shopImage[indexPath.row]
        cell.shopNameLbl.text = shop.name!
        cell.shopAddLbl.text = shop.address!
        
        
        return cell
    }
    
}

